const fs = require('fs');

const { Shop, Item } = require('../src/gilded_rose.js');

const { Runner } = require('./runner.js');

function readFileAsync(file_path) {
  return new Promise((resolve, reject) => {
    fs.readFile(file_path, (err, data) => {
      if (err) reject(err);
      
      resolve(data.toString());
    });
  });
}

describe("Gilded Rose", () => {
  describe("a complete execution", () => {
    it("is compliant to the golden master", (done) => {
      const ouputFile = './spec/output.txt';
      const goldenMasterFile = './spec/golden_master.txt';

      Promise.all([
        new Runner().run(ouputFile),
        readFileAsync(goldenMasterFile),
        readFileAsync(ouputFile)
      ])
      .then(([_, goldenMaster, output]) => {
        expect(output).toBe(goldenMaster);
        done();
      });
    });
  });
});

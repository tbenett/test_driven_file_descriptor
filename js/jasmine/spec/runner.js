const fs = require('fs');
const util = require('util');

const { Shop, Item } = require('../src/gilded_rose');

class Runner {
  constructor() {
    this.DAYS = 50;
  }
  
  run(output_file) {
    const items = [
      new Item("+5 Dexterity Vest", 10, 20),
      new Item("Aged Brie", 2, 0),
      new Item("Elixir of the Mongoose", 5, 7),
      new Item("Sulfuras, Hand of Ragnaros", 0, 80),
      new Item("Sulfuras, Hand of Ragnaros", -1, 80),
      new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
      new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
      new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
      // this conjured item does not work properly yet
      new Item("Conjured Mana Cake", 3, 6)
    ];
    
    const app = new Shop(items);
    let output = "";
    
    for (let day = 0; day < this.DAYS; ++day) {
      output += `-------- day ${day} --------\nname, sellIn, quality\n`;
      
      items.forEach(item => output += `${util.inspect(item)}\n`);
      
      output += '\n';
      app.updateQuality();
    }
    
    return this.writeFile(output_file, output);
  }

  writeFile(output_file, output) {
    return new Promise((resolve, reject) => {
      fs.writeFile(output_file, output, (err) => {
        if (err) reject(err);
        
        resolve();
      });
    });
  }
}

module.exports = {
  Runner
};
package com.gildedrose;

import org.junit.Test;

import java.io.*;

import static org.assertj.core.api.Assertions.assertThat;

public class GildedRoseTest {

    @Test
    public void output_is_compliant_to_golden_master() {
        Runner.run();

        try (final InputStream output = new FileInputStream("output.txt");
             final InputStream goldenMaster = new FileInputStream("golden_master.txt")) {
            assertThat(output).hasSameContentAs(goldenMaster);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
